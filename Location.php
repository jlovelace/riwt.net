<?php
class Location {
  private $lat;
  private $lon;

  public function __construct($lat, $lon){
    $this->lat = $lat;
    $this->lon = $lon;
  }
  public function get_lat() {
    return $this->lat;
  }

  public function get_lon() {
    return $this->lon;
  }
}
