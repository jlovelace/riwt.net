<?php
  // If upload button is clicked ...
  if (isset($_POST['upload'])) {

    // image file directory
	$filename = $FILES["image"]["name"];
	$tempname = $_FILES["image"]["tmp_name"];
  	$target = "~/up/".$filename;
	//basename($_FILES['image']['name']);
    $db = mysqli_connect("localhost", "root", "password" /* password here */, "regbusiness");

    // Get image name
    $bname = $_POST['bname'];
    $address = $_POST['address'];
    $website = $_POST['website'];
  	$image = $_FILES['image']['name'];

  	$sql = "INSERT INTO images (bname, address, website, image) VALUES ('$bname', '$address', '$website', '$image')";
  	mysqli_query($db, $sql);

  	if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
  		$msg = "Image uploaded successfully";
  	}else{
  		$msg = "Failed to upload image";
  	}
  }
?>
<!DOCTYPE html>
<html>
<head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right: 1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}

h1 {
  display: inline;
  font-color: #00B7AB;
}

#content{
 width: 50%;
 margin: 20px auto;
 border: 1px solid #cbcbcb;
}
form{
 width: 50%;
 margin: 20px auto;
}
form div{
 margin-top: 5px;
}
#img_div{
 width: 80%;
 padding: 5px;
 margin: 15px auto;
 border: 1px solid #cbcbcb;
}
#img_div:after{
 content: "";
 display: block;
 clear: both;
}
img{
 float: left;
 margin: 5px;
 width: 300px;
 height: 140px;
}
footer {
  position: relative;
  bottom: 0px;
  background-color: #333;
  height: 60px;
  width: 100%;
}

footer a:first-child {
  position: relative;
  left: 33%;
  color: #00b8ad;
}

footer img {
  position: relative;
  left: 50%;
  width: 50px;
  height: 50px;
}

footer a:last-child {
  position: relative;
  left: 60%;
  color: #00b8ad;
}
</style>
<title>Local Business</title>
</head>

<body style="background-color:powderblue;">
<h1>riwt.net</h1>
<ul>
  <li><a href="index.php">About</a></li>
  <li><a class="active" href="business.php">Local Business</a></li>
  <li><a href="music.php">Local Music</a></li>
  <li><a href="links.html">Links</a></li>
  <li><a href="app.php">Web App</a></li>
</ul>
  <div id="content">
    <h1 style="text-align:center;">Register Business</h1>
    <form method="post" action="business.php" enctype="multipart/form-data">
      <input type="hidden" name="size" value="1000000">
      <div>
        Business Name: <input type="text" name="bname">
      </div>
      <div>
        Address: <input type="text" name="address">
      </div>
      <div>
        Website: <input type="text" name="website">
      </div>
      <div>
        Picture: <input type="file" name="image">
      </div>
      <div>
        <input type="submit" name="upload" value="Register">
      </div>
    </form>
    <?php
        /*  $db = mysqli_connect("localhost", "root", "password" , "regbusiness");
          $sql = "SELECT * FROM images";
          $result = mysqli_query($db, $sql);
          while($row = mysqli_fetch_array($result)) {
            echo "<div id='img_div'>";
              echo "<img src='images/".$row['image']."' >";
              echo "<div>";
              echo "Business: ";
              echo $row['bname'];
              echo "</div>";
              echo "<div>";
              echo "Address: ";
              echo $row['address'];
              echo "</div>";
              echo "<div>";
              echo "Website: ";
              echo "<a href='";
              echo $row['website'];
              echo "'>";
              echo $row['website'];
              echo "</a>";
              echo "</div>";
            echo "</div>";
            echo "<br>";
            echo "<br>";
          }
          echo "<br>";
  */  ?>
  </div>
</body>
<footer>
  <a href="legal.html">legal</a>
  <img src="logo.png" alt="Riwt.net logo" height="80" width="80">
  <a href="mailto: jlovelace@mines.edu">contact</a>
</footer>
</html>
