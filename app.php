<?php
include "config.php";

if(isset($_POST['but_submit'])){

    $uname = mysqli_real_escape_string($con,$_POST['txt_uname']);
    $password = mysqli_real_escape_string($con,$_POST['txt_pwd']);
    $login_error_msg;

    if ($uname != "" && $password != ""){

        $sql_query = "select count(*) as cntUser from users where username='".$uname."' and password='".$password."'";
        $result = mysqli_query($con,$sql_query);
        $row = mysqli_fetch_array($result);

        $count = $row['cntUser'];

        if($count > 0){
            $_SESSION['uname'] = $uname;
            header('Location: inside.php');
        }else{
            $login_error_msg = "Invalid username and password";
        }

    }
}

if(isset($_POST['guest_submit'])){
	$uname = "guest";
	$_SESSION['uname'] = $uname;
	header('Location: inside.php');
}
?>

<!DOCTYPE html>
<html>
<head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right: 1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}

h1 {
  display: inline;
  font-color: #00B7AB;
}
footer {
  position: relative;
  bottom: 0px;
  background-color: #333;
  height: 60px;
  width: 100%;
}

footer a:first-child {
  position: relative;
  left: 33%;
  color: #00b8ad;
}

footer img {
  position: relative;
  left: 50%;
  width: 50px;
  height: 50px;
}

footer a:last-child {
  position: relative;
  left: 60%;
  color: #00b8ad;
}


.container{
    width:40%;
    margin:0 auto;
}

#div_login{
    border: 1px solid gray;
    border-radius: 3px;
    width: 470px;
    height: 270px;
    box-shadow: 0px 2px 2px 0px  gray;
    margin: 0 auto;
}

#div_login h1{
    margin-top: 0px;
    font-weight: normal;
    padding: 10px;
    background-color: cornflowerblue;
    color: white;
    font-family: sans-serif;
}

#div_login div{
    clear: both;
    margin-top: 10px;
    padding: 5px;
}

#div_login .textbox{
    width: 96%;
    padding: 7px;
}

#div_login input[type=submit]{
    padding: 7px;
    width: 100px;
    background-color: lightseagreen;
    border: 0px;
    color: white;
}

#div_login button[type=submit]{
	padding: 7px;
	width: 100px;
	background-color: lightseagreen;
	border: 0px;
	color: white;
}
</style>
<?php
$error_message = "";$success_message = "";

// Register user
if(isset($_POST['btnsignup'])){
   $username = trim($_POST['username']);
   $password = trim($_POST['password']);
   $confirmpassword = trim($_POST['confirmpassword']);

   $isValid = true;

   // Check fields are empty or not
   if($username == '' ||  $password == '' || $confirmpassword == ''){
     $isValid = false;
     $error_message = "Please fill all fields.";
   }

   // Check if confirm password matching or not
   if($isValid && ($password != $confirmpassword) ){
     $isValid = false;
     $error_message = "Confirm password not matching";
   }
/*
   // Check if Email-ID is valid or not
   if ($isValid && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
     $isValid = false;
     $error_message = "Invalid Email-ID.";
   }
*/
   if($isValid){

     // Check if Email-ID already exists
     $stmt = $con->prepare("SELECT * FROM users WHERE username = ?");
     $stmt->bind_param("s", $username);
     $stmt->execute();
     $result = $stmt->get_result();
     $stmt->close();
     if($result->num_rows > 0){
       $isValid = false;
       $error_message = "Username is already existed.";
     }

   }

   // Insert records
   if($isValid){
     $insertSQL = "INSERT INTO users(username,password ) values(?,?)";
     $stmt = $con->prepare($insertSQL);
     $stmt->bind_param("ss",$username,$password);
     $stmt->execute();
     $stmt->close();

     $success_message = "Account created successfully.";
   }
}
?>
</head>

<body style="background-color:powderblue;">
<h1>riwt.net</h1>
<ul>
  <li><a href="index.php">About</a></li>
  <li><a href="business.php">Local Business</a></li>
  <li><a href="music.php">Local Music</a></li>
  <li><a href="links.html">Links</a></li>
  <li><a class="active" href="app.php">Web App</a></li>
</ul>

<div class="container">
    <form method="post" action="">
        <div id="div_login">
	    <h1>Login</h1>
		<?php
		//Display Error Msg
		if(!empty($login_error_msg)){
		?>
		<div class="alert alert-danger">
			<strong>Error!</strong> <?= $login_error_msg ?>
		</div>
		<?php
		}
		?>
            <div>
                <input type="text" class="textbox" id="txt_uname" name="txt_uname" placeholder="Username" />
            </div>
            <div>
                <input type="password" class="textbox" id="txt_uname" name="txt_pwd" placeholder="Password"/>
            </div>
            <div>
		<input type="submit" value="Login" name="but_submit" id="but_submit" />
		<span>Or</span>
		<input type="submit" value="Login as Guest" name="guest_submit" id="guest_submit" />
            </div>
        </div>
    </form>
</div>
<p><center>Or</center></p>
<div class='container'>
      <div class='row'>

        <div class='col-md-6' >

          <form method='post' action=''>
	  	<div id="div_login">
            	<h1>SignUp</h1>
            	<?php
            	// Display Error message
            	if(!empty($error_message)){
            	?>
            	<div class="alert alert-danger">
              	<strong>Error!</strong> <?= $error_message ?>
            	</div>

            	<?php
            	}
            	?>

            	<?php
            	// Display Success message
            	if(!empty($success_message)){
            	?>
            	<div class="alert alert-success">
              	<strong>Success!</strong> <?= $success_message ?>
            	</div>

            	<?php
            	}
            	?>

            	<div class="form-group">
              	<label for="username">Username:</label>
              	<input type="text" class="form-control" name="username" id="username" required="required" maxlength="80">
            	</div>
            	<div class="form-group">
              	<label for="password">Password:</label>
              	<input type="password" class="form-control" name="password" id="password" required="required" maxlength="80">
            	</div>
            	<div class="form-group">
              	<label for="pwd">Confirm Password:</label>
              	<input type="password" class="form-control" name="confirmpassword" id="confirmpassword" onkeyup='' required="required" maxlength="80">
            	</div>

		<button type="submit" name="btnsignup" class="btn btn-default">Submit</button>
	  </div>
          </form>
        </div>

     </div>
    </div></body>
<footer>
  <a href="legal.html">legal</a>
  <img src="logo.png" alt="Riwtwir logo" height="80" width="80">
  <a href="mailto: jlovelace@mines.edu">contact</a>
</footer>
