<?php
//editted for example not correct riwt
//require_once("session_start.php");
//include 'Location.php';
class Riwt {
  private $startLoc;
  private $stopLoc;
  private $startTime;
  private $stopTime;
  private $timeDiff;
  private $distance;
  private $username;

public function __construct($startLoc, $stopLoc, $startTime, $stopTime, $username) {
    $this->startLoc = $startLoc;
    $this->stopLoc = $stopLoc;
    $this->startTime = $startTime / 1000;
    $this->stopTime = $stopTime / 1000;
    $this->username = $username;
    $this->distance();
    $this->timeDiff();
    $this->storeRiwt();
  }

//Haversine Formula Calc Distance b/w points
private function distance() {
  $earthRadius = 6371000;
  // convert from degrees to radians
  $latFrom = deg2rad($this->startLoc->get_lat());
  $lonFrom = deg2rad($this->startLoc->get_lon());
  $latTo = deg2rad($this->stopLoc->get_lat());
  $lonTo = deg2rad($this->stopLoc->get_lon());
  $lonDelta = $lonTo - $lonFrom;
  $a = pow(cos($latTo) * sin($lonDelta), 2) +
    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

  $angle = atan2(sqrt($a), $b);
  $this->distance = $angle * $earthRadius;
  $this->distance = $this->distance / 1609;
}

//calculate elapse time from start to stop
private function timeDiff() {
  $this->timeDiff = $this->stopTime - $this->startTime;
}

//store in database
private function storeRiwt() {

  $conn = mysqli_connect("localhost", "root", "password, "riwt");

  // Check connection
  if($conn === false){
      die("ERROR: Could not connect. "
          . mysqli_connect_error());
  }

  $startLat = $this->startLoc->get_lat();
  $startLon = $this->startLoc->get_lon();
  $stopLat = $this->stopLoc->get_lat();
  $stopLon = $this->stopLoc->get_lon();
  $startTime = $this->startTime;
  $stopTime = $this->stopTime;
  $timeDiff = $this->timeDiff;
  $distance = $this->distance;
  $username = $this->username;
  $sql = "INSERT INTO riwt (startLat, startLon, stopLat, stopLon, startTime, stopTime, timeDiff, distance, username) VALUES
  ('$startLat', '$startLon', '$stopLat', '$stopLon', '$startTime', '$stopTime', '$timeDiff', '$distance', '$username')";

  if(mysqli_query($conn, $sql)){
    echo "<h>Success! Go Back and Reload.</h>";
  } else{
      echo "<h>error</h>";
  }

  // Close connection
  mysqli_close($conn);

}

//getters & setters
public function getStartLoc(){
  return $this->startLoc;
}
public function getStopLoc(){
  return $this->stopLoc;
}
public function getStartTime(){
  return $this->startTime;
}
public function getStopTime(){
  return $this->stopTime;
}
public function getUserName(){
  return $this->username;
}
public function getTimeDiff(){
  return $this->timeDiff;
}
public function getDistance(){
  return $this->distance;
}
}
