<?php
  // If upload button is clicked ...
  if (isset($_POST['upload'])) {

    // image file directory
  	$target = "images/".basename($_FILES['image']['name']);
    $db = mysqli_connect("localhost", "root", "password", "regbusiness");

    // Get image name
    $bname = $_POST['bname'];
    $address = $_POST['address'];
    $website = $_POST['website'];
  	$image = $_FILES['image']['name'];

  	$sql = "INSERT INTO images (bname, address, website, image) VALUES ('$bname', '$address', '$website', '$image')";
  	mysqli_query($db, $sql);

  	if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
  		$msg = "Image uploaded successfully";
  	}else{
  		$msg = "Failed to upload image";
  	}
  }
?>

<?php
  // If upload button is clicked ...
  if (isset($_POST['sendprayer'])) {
    $db = mysqli_connect("localhost", "root", "password", "prayers");

    $prayer = $_POST['prayer'];

        $sql = "INSERT INTO prayerlist (prayer) VALUES ('$prayer')";
        mysqli_query($db, $sql);
  }
?>


<!DOCTYPE html>
<html>
<head>
<style type="text/css">
ul.list-style {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li.element {
  float: left;
  border-right: 1px solid #bbb;
}

li.element:last-child {
  border-right: none;
}

li.element a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li.element a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}

h1 {
  display: inline;
  font-color: #00B7AB;
}

h2 {
  font-size: 25px;
  border-left: 3px solid #bada55;
}

#content{
 width: 50%;
 margin: 20px auto;
 border: 1px solid #cbcbcb;
}
form{
 width: 50%;
 margin: 20px auto;
}
form div{
 margin-top: 5px;
}
#img_div{
 width: 80%;
 padding: 5px;
 margin: 15px auto;
 border: 1px solid #cbcbcb;
}
#img_div:after{
 content: "";
 display: block;
 clear: both;
}
img{
 float: left;
 margin: 5px;
 width: 300px;
 height: 140px;
}

footer {
  position: relative;
  bottom: 0px;
  background-color: #333;
  height: 60px;
  width: 100%;
}

footer a:first-child {
  position: relative;
  left: 33%;
  color: #00b8ad;
}

footer img {
  position: relative;
  left: 50%;
  width: 50px;
  height: 50px;
}

footer a:last-child {
  position: relative;
  left: 60%;
  color: #00b8ad;
}

</style>
<title>riwt.net</title>
</head>
<body style="background-color:powderblue;">
<div>
  <h1>riwt.net</h1>
</div>
<ul class="list-style">
  <li class="element"><a class="active" href="index.php">About</a></li>
  <li class="element"><a href="business.php">Local Business</a></li>
  <li class="element"><a href="music.php">Local Music</a></li>
  <li class="element"><a href="links.html">Links</a></li>
  <li class="element"><a href="app.php">Web App</a></li>
</ul>
<h2>what is riwt.net?</h2>
<p>Riwt.net is a self propulsion delivery and commuting platform supporting local business and music! Riwt.net encourages individuals to frequent local destinations in creative human powered ways like walking, running, biking, skating, etc. Riwt.net is open source to encourage active involvement and transparency of the community; all modifications and redistributions are welcome. Check the links tab to help build the community. Riwt.net is an abbreviation that stands for ride into work today.</p>
<p>Riwt.net's main contributor, Joseph Lovelace, is taking private prayer requests below. Mr. Lovelace would love to use his time to pray for you. God Bless!</p>
<h2>how to use riwt.net?</h2>
<p>The possibilites are endless! </p>
  <ul>
    <li>Listen to local music</li>
    <li>Frequent local business</li>
    <li>Commute to and from work</li>
    <li>Get outside</li>
    <li>Register a business on riwt.net</li>
    <li>Register a musician on riwt.net</li>
    <li>Encourage a business/musician to register on riwt.net</li>
    <li>Commit anything to online repository</li>
    <li>Create a new redistribution of riwt.net for another locality</li>
    <li>...</li>
  </ul>
  <div id="content">
    <h1 style="text-align:center;">Register Business</h1>
  <?php
  /*
        $db = mysqli_connect("localhost", "root", "password", "regbusiness");
        $sql = "SELECT * FROM images";
        $result = mysqli_query($db, $sql);
        while($row = mysqli_fetch_array($result)) {
          echo "<div id='img_div'>";
            echo "<img src='images/".$row['image']."' >";
            echo "<div>";
            echo "Business: ";
            echo $row['bname'];
            echo "</div>";
            echo "<div>";
            echo "Address: ";
            echo $row['address'];
            echo "</div>";
            echo "<div>";
            echo "Website: ";
            echo "<a href='";
            echo $row['website'];
            echo "'>";
            echo $row['website'];
            echo "</a>";
            echo "</div>";
          echo "</div>";
          echo "<br>";
          echo "<br>";
        }
        echo "<br>";
        */
  ?>
    <form method="post" action="index.php" enctype="multipart/form-data">
    	<input type="hidden" name="size" value="1000000">
      <div>
        Business Name: <input type="text" name="bname">
      </div>
      <div>
        Address: <input type="text" name="address">
      </div>
      <div>
        Website: <input type="text" name="website">
      </div>
    	<div>
        Picture: <input type="file" name="image">
    	</div>
    	<div>
    		<input type="submit" name="upload" value="Register">
    	</div>
    </form>
  </div>
  <div id="content">
  <h1 style="text-align:center;">Prayer Request</h1>
   <form method="post" action="index.php" enctype="multipart/form-data">
        <input type="hidden" name="size" value="1000000">
      <div>
        Prayer Intention: <input type="text" name="prayer">
      </div>
        <div>
                <input type="submit" name="sendprayer" value="Send Prayer Request">
        </div>
    </form>
  </div>
</body>
<footer>
  <a href="legal.html">legal</a>
  <img src="logo.png" alt="Riwt.net logo" height="80" width="80">
  <a href="mailto:jlovelace@mines.edu">Contact</a>
</footer>
</html>
