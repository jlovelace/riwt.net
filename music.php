<?php
  // If upload button is clicked ...
  if (isset($_POST['upload'])) {

    // image file directory
  	$target = "audio/".basename($_FILES['file']['name']);
    $db = mysqli_connect("localhost", "root", "password", "regmusic");

    // Get image name
    $mname = $_POST['mname'];
    $website = $_POST['website'];
  	$file = $_FILES['file']['name'];

  	$sql = "INSERT INTO audio (mname, website, file) VALUES ('$mname', '$website', '$file')";
  	mysqli_query($db, $sql);

  	if (move_uploaded_file($_FILES['file']['tmp_name'], $target)) {
  		$msg = "File uploaded successfully";
  	}else{
  		$msg = "Failed to upload file";
  	}
  }
?>
<!DOCTYPE html>
<html>
<head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right: 1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}

h1 {
  display: inline;
  font-color: #00B7AB;
}

.artist {
  border: double;
  text-align: center;
}

.artist audio {
  display: block;
  margin-left: auto;
  margin-right: auto;
}

#name {
  text-align: left;
  padding-left: 5px;
}

#content{
 width: 50%;
 margin: 20px auto;
 border: 1px solid #cbcbcb;
}
form{
 width: 50%;
 margin: 20px auto;
}
form div{
 margin-top: 5px;
}
#img_div{
 width: 80%;
 padding: 5px;
 margin: 15px auto;
 border: 1px solid #cbcbcb;
}
#img_div:after{
 content: "";
 display: block;
 clear: both;
}
img{
 float: left;
 margin: 5px;
 width: 300px;
 height: 140px;
}
footer {
  position: relative;
  bottom: 0px;
  background-color: #333;
  height: 60px;
  width: 100%;
}

footer a:first-child {
  position: relative;
  left: 33%;
  color: #00b8ad;
}

footer img {
  position: relative;
  left: 50%;
  width: 50px;
  height: 50px;
}

footer a:last-child {
  position: relative;
  left: 60%;
  color: #00b8ad;
}

</style>
<title>Local Music</title>
</head>
<body style="background-color:powderblue;">
<h1>riwt.net</h1>
<ul>
  <li><a href="index.php">About</a></li>
  <li><a href="business.php">Local Business</a></li>
  <li><a class="active" href="music.php">Local Music</a></li>
  <li><a href="links.html">Links</a></li>
  <li><a href="app.php">Web App</a></li>
</ul>
<div id="content">
  <h1 style="text-align:center;">Register Artist</h1>
  <form method="post" action="music.php" enctype="multipart/form-data">
    <input type="hidden" name="size" value="1000000">
    <div>
      Artist Name: <input type="text" name="mname">
    </div>
    <div>
      Website: <input type="text" name="website">
    </div>
    <div>
      Audio Upload: <input type="file" name="file">
    </div>
    <div>
      <input type="submit" name="upload" value="Register">
    </div>
  </form>
  <div class="artist">
    <p id="name">Featured Artist: Henry Lovelace</p>
    <p>Song: "Get Off the Net"</p>
    <audio src="Get Off The Net MIX1.mp3" controls="controls">
      Your browser does not support the audio element.
    </audio>
    <p>Song: "Change of Scenery"</p>
    <audio src="Change Of Scenery MIX1.mp3" controls="controls">
      Your browser does not support the audio element.
    </audio>
    <p>Song: "Heaven"</p>
    <audio src="Heaven MIX1.mp3" controls="controls">
      Your browser does not support the audio element.
    </audio>
  </div>
  <?php
        /*
        $db = mysqli_connect("localhost", "root", "password", "regmusic");
        $sql = "SELECT * FROM audio";
        $result = mysqli_query($db, $sql);
        while($row = mysqli_fetch_array($result)) {
          echo "<div id='img_div'>";
            echo "<div>";
            echo "Artist: ";
            echo $row['mname'];
            echo "</div>";
            echo "<div>";
            echo "Website: ";
            echo "<a href='";
            echo $row['website'];
            echo "'>";
            echo $row['website'];
            echo "</a>";
            echo "</div>";
            echo "<div>";
            echo "Song: ";
            echo "<audio src='audio/".$row['file']."' controls='controls'>";
            echo "</div>";
          echo "</div>";
          echo "<br>";
          echo "<br>";
        }
        echo "<br>";
        */
  ?>
</div>
</body>
<footer>
  <a href="legal.html">legal</a>
  <img src="logo.png" alt="Riwtwir logo" height="80" width="80">
  <a href="mailto: jlovelace@mines.edu">contact</a>
</footer>
</html>
