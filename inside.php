<?php
include "config.php";
//check user login
if(!isset($_SESSION['uname'])){
	header('Location: app.php');
}

//logout
if(isset($_POST['but_logout'])){
	session_destroy();
	header('Location: app.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right: 1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}

h1 {
  display: inline;
  font-color: #00B7AB;
}
footer {
  position: relative;
  bottom: 0px;
  background-color: #333;
  height: 60px;
  width: 100%;
}

footer a:first-child {
  position: relative;
  left: 33%;
  color: #00b8ad;
}

footer img {
  position: relative;
  left: 50%;
  width: 50px;
  height: 50px;
}

footer a:last-child {
  position: relative;
  left: 60%;
  color: #00b8ad;
}

table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>

<body style="background-color:powderblue;">
<h1>riwt.net</h1>
<ul>
  <li><a href="index.php">About</a></li>
  <li><a href="business.php">Local Business</a></li>
  <li><a href="music.php">Local Music</a></li>
  <li><a href="links.html">Links</a></li>
  <li><a class="active" href="app.php">Web App</a></li>
</ul>
<form method='post' action="">
	<input type="submit" value="Logout" name="but_logout">
</form>
<p id="demo">Click the button to get your coordinates:</p>
<p>Start Lat: <span id="rlat"></span></p>
<p>Start Lon: <span id="rlon"></span></p>
<p>Stop Lat: <span id="olat"></span></p>
<p>Stop Lon: <span id="olon"></span></p>
<button onclick="getLocationStart()">Start Riwt</button>
<button onclick="getLocationStop()">Stop Riwt</button>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
	var x = document.getElementById("demo");
	var hitStart = false;

	function getLocationStart()
	{
		if (navigator.geolocation)
		{
			navigator.geolocation.getCurrentPosition(bindStartPosition);
		}
		else {
			x.innerHTML = "Geolocation is not supported by this browser.";
		}
	}
	function bindStartPosition(position) {
		hitStart = true;
		var start = + new Date();
		document.getElementById("rlat").innerHTML = position.coords.latitude;
		document.getElementById("rlon").innerHTML = position.coords.longitude;
		$("input[name='startlat']").val(position.coords.latitude);
		$("input[name='startlon']").val(position.coords.longitude);
		$("input[name='startdate']").val(start);
	}

	function getLocationStop()
	{
		if (navigator.geolocation && hitStart)
		{
			navigator.geolocation.getCurrentPosition(bindStopPosition);
		}
		else {
			x.innerHTML = "Geolocation is not supported by this browser or Hit Start Button First";
		}
	}
	function bindStopPosition(position) {
		var stop = + new Date();
		document.getElementById("olat").innerHTML = position.coords.latitude;
		document.getElementById("olon").innerHTML = position.coords.longitude;
		$("input[name='stoplat']").val(position.coords.latitude);
		$("input[name='stoplon']").val(position.coords.longitude);
		$("input[name='stopdate']").val(stop);
	}
</script>
<form id="form1" name="form1" method="post" action="riwttest.php">
	<p>
		<input type='hidden' value='' name='startlat'/>
		<input type='hidden' value='' name='startlon'/>
		<input type='hidden' value='' name='startdate'/>
		<input type='hidden' value='' name='stoplat'/>
		<input type='hidden' value='' name='stoplon'/>
		<input type='hidden' value='' name='stopdate'/>
		<input type="submit" name="submit" id="submit" value="Submit Riwt" />
	</p>
</form>
<?php
$db = mysqli_connect("localhost", "root", "password", "riwt");
$sql = "SELECT * FROM riwt";
$result = mysqli_query($db, $sql);
$num = 1;
echo '<table style="width:100%">';
echo '<tr>';
echo '<th>Riwt #</th>';
echo '<th>Elapsed Time [s]</th>';
echo '<th>Distance [miles]</th>';
echo '<th>Start Time</th>';
echo '<th>Stop Time</th>';
echo '</tr>';
while($row = mysqli_fetch_array($result)) {
	if($_SESSION['uname'] == $row['username']){
		$start = new \DateTime();
		$start->setTimeStamp($row['startTime']);
		$stop = new \DateTime();
		$stop->setTimeStamp($row['stopTime']);
		echo '<tr>';
		echo '<th>';
		echo $num;
		echo '</th>';
		echo '<th>';
		echo gmdate("H:i:s", $row['timeDiff']);
		echo '</th>';
		echo '<th>';
		echo $row['distance'];
		echo '</th>';
		echo '<th>';
		echo $start->format('Y-m-d H:i:s');
		echo '</th>';
		echo '<th>';
		echo $stop->format('Y-m-d H:i:s');
		echo '</th>';
		echo '</tr>';
		$num = $num + 1;
	}
}
echo '</table>';
 ?>

</body>
<footer>
  <a href="legal.html">legal</a>
  <img src="logo.png" alt="Riwtwir logo" height="80" width="80">
  <a href="mailto: jlovelace@mines.edu">contact</a>
</footer>
</html>
